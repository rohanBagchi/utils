#!/bin/zsh

RECRUITERBOX_MAIN="recruiterbox_main"

if [ "`git branch --list ${RECRUITERBOX_MAIN}`" ]
then
	echo "checking out recruiterbox_main"
	git checkout recruiterbox_main
else
	echo "checking out master"
	git checkout master
fi
