# Steps

1. Add the following block to `.git/hooks/prepare-commit-msg`
2. Then add execution permission to the file with chmod: `chmod +x .git/hooks/prepare-commit-msg`

Now, as long as the branch name has the ticket number, the commit message will be prepended with the ticket number. 


```sh
#!/bin/bash

# Define your JIRA ticket pattern, e.g., "PROJECT-1234"
JIRA_PATTERN="[A-Za-z]+-[0-9]+"

# Extract the current branch name
BRANCH_NAME=$(git rev-parse --abbrev-ref HEAD)

# Extract JIRA ticket number from the branch name
JIRA_TICKET=$(echo $BRANCH_NAME | grep -o -E $JIRA_PATTERN)

# Path to the commit message file
COMMIT_MSG_FILE=$1

# Read the commit message
COMMIT_MSG=$(cat $COMMIT_MSG_FILE)

# Check if the commit message already contains the JIRA ticket number
if ! echo "$COMMIT_MSG" | grep -q -E $JIRA_PATTERN; then
  # Prepend the JIRA ticket number to the commit message
  if [ -n "$JIRA_TICKET" ]; then
    JIRA_TICKET_UPPERCASE=$(echo "$JIRA_TICKET" | tr '[:lower:]' '[:upper:]')
    echo "$JIRA_TICKET_UPPERCASE: $COMMIT_MSG" > $COMMIT_MSG_FILE
  fi
fi


```