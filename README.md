# utils
Repository of utility functions

- checkout_master.sh : Looks if `recruiterbox_main` is present. If so, checks out `recruiterbox_main` else checks out `master`
- cleanup.sh : Finds branches that are merged to master and cleans them up

Create a `repo` folder under home with `mkdir repo && cd repo`. 
Clone this repository.

To paste the live templates

In the Live Templates page of the Settings/Preferences dialog, select the group where you want to insert the copied live templates.
Right-click the selection, and choose Paste on the context menu.
An XML copy of the selected templates is inserted from the system clipboard to the selected group of live templates.


#### Installing SublimeText
1. Download latest
2. run `ln -s '/Applications/Sublime Text.app/Contents/SharedSupport/bin/subl' /usr/local/bin/subl` in terminal
3. Install `AutoFileName` to help imports
4. Install `AdvancedNewFile` to add new file `cmd+shift+n`